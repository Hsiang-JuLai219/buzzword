package buzzword;

import apptemplate.AppTemplate;
import components.AppComponentsBuilder;
import components.AppDataComponent;
import components.AppFileComponent;
import components.AppWorkspaceComponent;
import data.GameData;
import files.GameFile;
import gui.Workspace;

/**
 * @author Hsiang-Ju Lai
 */
public class BuzzWord extends AppTemplate {
    @Override
    public AppComponentsBuilder makeAppBuilderHook() {
        return new AppComponentsBuilder() {
            @Override
            public AppDataComponent buildDataComponent() throws Exception {
                return new GameData();
            }

            @Override
            public AppFileComponent buildFileComponent() throws Exception {
                return new GameFile(BuzzWord.this);
            }

            @Override
            public AppWorkspaceComponent buildWorkspaceComponent() throws Exception {
                return new Workspace(BuzzWord.this);
            }
        };

    }

    public static void main(String[]args){launch(args);}
}
