package controller;

import components.AppFileComponent;
import data.GameMode;
import data.UserData;
import files.AccountList;
import files.GameFile;
import gui.HomeScreen;
import gui.LoginPopUp;
import gui.Workspace;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;

import static buzzword.Properties.INVALID_PASSWORD_MESSAGE;
import static buzzword.Properties.USER_PROFILES_DIRECTORY;

/**
 * @author Hsiang-Ju Lai
 */
public class HomeController {

    private Workspace workspace;
    private HomeScreen screen;
    private AccountList accounts;
    private boolean isLoggedIn;
    private UserData user;
    private GameFile fileController;
    private GameMode selectedMode;

    public HomeController(HomeScreen screen, AppFileComponent fileComponent, Workspace workspace)
    {
        this.screen = screen;
        this.fileController = (GameFile)fileComponent;
        this.workspace = workspace;
        accounts = AccountList.getSingleton();
}

    public void login(){
        if(isLoggedIn)
            logout();
        else
        {
            LoginPopUp pop = LoginPopUp.getSingleton();
            pop.showAndWait();
            if(pop.login())
            {
                PropertyManager pm = PropertyManager.getManager();
                String username = pop.getUsername();
                String password = pop.getPassword();

                if(!accounts.checkPassword(username, password))
                {

                    AppMessageDialogSingleton.getSingleton().show("", pm.getPropertyValue(INVALID_PASSWORD_MESSAGE));
                    return;
                }

                user = new UserData(username);
                fileController.loadData(user, pm.getPropertyValue(USER_PROFILES_DIRECTORY) + username + ".json");

                isLoggedIn = true;
                screen.updateControlPane(isLoggedIn, username);
            }
        }

    }

    private void logout()
    {
        user = null;
        selectedMode = null;
        isLoggedIn = false;
        screen.updateControlPane(false, null);
    }

    public void profileSetting()
    {
        workspace.toProfileSettings(isLoggedIn, user);
    }

    public void levelSelection()
    {
        if(selectedMode != null)
            workspace.toLevelSelection(selectedMode, user);
    }

    public void help(){workspace.toHelpScreen();}

    public void initMenuItemHandlers(MenuItem[] items)
    {
        for(int i = 0; i < GameMode.values().length; i++)
            items[i].setOnAction(new MenuItemHandler(i));
    }

    private void setGameMode(GameMode mode)
    {
        selectedMode = mode;
        screen.modeSelected(mode);
        screen.enableStartGame();
    }

    public boolean isLoggedIn(){return isLoggedIn;}

    public GameMode getSelectedMode() {return selectedMode;}

    class MenuItemHandler implements EventHandler<ActionEvent>{
        private int pos;
        public MenuItemHandler(int pos){this.pos = pos;}

        @Override
        public void handle(ActionEvent event) {
            setGameMode(GameMode.values()[pos]);
        }
    }

}


