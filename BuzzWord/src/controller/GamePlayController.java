package controller;

import data.Connection;
import data.GameData;
import data.GameMode;
import data.UserData;
import files.GameFile;
import gui.GamePlay;
import gui.LevelSelection;
import gui.Workspace;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.ObservableMap;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.util.ArrayList;

import static buzzword.Properties.*;


/**
 * @author Hsiang-Ju Lai
 */
public class GamePlayController {

    Workspace workspace;
    private GamePlay screen;
    private boolean isEnded;
    private boolean isPaused;
    private Timeline timeline;
    GameData data;
    UserData user;


    public GamePlayController(Workspace workspace, GamePlay screen, GameData data, UserData user)
    {
        this.workspace = workspace;
        this.screen = screen;
        this.data = data;
        this.user = user;
    }

    public void reset(UserData user)
    {
        isPaused = false;
        this.user = user;
    }


    //case 8
    public void returnToHomeScreen()
    {
        pause();
        YesNoCancelDialogSingleton dialog = YesNoCancelDialogSingleton.getSingleton();
        dialog.disableCancel(true);
        dialog.show("", PropertyManager.getManager().getPropertyValue(QUIT_GAME));
        if(dialog.getSelection().equals("Yes"))
        {
            workspace.toHomeScreen();
        }
        else
            resume();
    }

    public void startPlaying()
    {
        isEnded = false;
        screen.disableResumeButton(false);
        data.setTargetWords();
        screen.initLetterGrid();
        screen.resetGridEffect();
        timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1),
                event -> {
                    if(screen.countDown())
                        end();
                }));
        timeline.play();
    }

    public void profileSettings()
    {
        pause();
        workspace.toProfileSettings(true, user);
    }

    //case 9
    public void pauseResumeGame()
    {
        if(isPaused)
            resume();
        else
            pause();
    }

    public void setPaused(boolean isPaused)
    {
        if(isPaused)
            pause();
        else
            resume();
    }

    private void resume()
    {
        if(isEnded)
            return;

        isPaused = false;
        screen.setGridVisible(true);
        timeline.play();
        screen.updatePauseResumeButton(isPaused);
        screen.requestFocus();
    }

    private void pause()
    {
        if(isEnded)
            return;

        isPaused = true;
        screen.setGridVisible(false);
        timeline.pause();
        screen.updatePauseResumeButton(isPaused);
    }

    //case 12
    public void replay()
    {
        timeline.stop();
        startPlaying();
        screen.reset();
        isPaused = false;
    }

    public void startNextLevel()
    {
        int nextLevel = data.getLevel() + 1;
        if(nextLevel <= LevelSelection.NUMBER_OF_LEVELS)
        {
            data.setLevel(nextLevel);
            replay();
        }
        else
            AppMessageDialogSingleton.getSingleton().show("", PropertyManager.getManager().getPropertyValue(NO_HIGHER));
    }


    //case 18
    public void end()
    {
        boolean isSucceed = data.getCurrentScore() >= data.getTargetScore();
        timeline.stop();
        screen.disableResumeButton(true);
        clearDisplay();
        screen.disableGamePlay(true);

        PropertyManager pm = PropertyManager.getManager();

        if(isSucceed)
            user.clearLevel(data.getMode().toString(), data.getLevel());

        boolean isPersonalBest = user.updateRecord(data.getMode().toString(), data.getLevel(), data.getCurrentScore());
        Platform.runLater(() ->
        {
            AppMessageDialogSingleton.getSingleton().show("", pm.getPropertyValue(isSucceed ? WIN_MESSAGE : LOSE_MESSAGE));
            if(isPersonalBest)
                AppMessageDialogSingleton.getSingleton().show("", pm.getPropertyValue(PERSONAL_BEST_MESSAGE));
        });
        GameFile fileController = (GameFile) workspace.getApp().getFileComponent();
        fileController.saveData(user, pm.getPropertyValue(USER_PROFILES_DIRECTORY) + user.getName() + ".json");

        screen.updateControlPane(isSucceed);
        data.showAnswers();
        isEnded = true;
    }

    private void updateGuessingWord(String newLetter)
    {
        screen.updateGuessingWord(screen.getGuessingWord() + newLetter + " ");

    }

    public void evaluateGuessingWord()
    {
        String word = screen.getGuessingWord().replaceAll(" ", "");
        if(data.checkWord(word))
        {
            data.addGuessed(word);
            screen.updateScore();
        }
        clearDisplay();
    }

    private void clearDisplay()
    {
        screen.updateGuessingWord("");
        screen.resetGridEffect();
        data.clearVisitedFlags();
        screen.requestFocus();
    }

    public int getTargetScore(){return data.getTargetScore();}

    public char[][] getLetterGrid(){return data.getCharGrid();}

    public String getUsername(){return user.getName();}

    public int getCurrentLevel(){return data.getLevel();}

    public GameMode getMode() {return data.getMode();}

    public ObservableMap<String, String> getGuessed() {return data.getGuessed();}

    public int getCurrentScore() {return data.getCurrentScore();}

    public boolean isPaused(){return isPaused;}

    public void setupGridHandler(TextField[][] letters)
    {
        for(int i = 0; i < screen.getNumberOfRows(); i++)
            for(int j = 0; j < screen.getNumberOfColumns(); j++)
            {
                letters[i][j].setOnMouseDragEntered(new SelectByMouseDragHandler(letters[i][j].getText(), i, j));
                letters[i][j].setOnMouseDragReleased(event -> evaluateGuessingWord());
                letters[i][j].setOnMousePressed(event -> clearDisplay());
            }
        screen.setOnMousePressed(event -> clearDisplay());
    }


    private void highlightPaths(ArrayList<Connection> paths)
    {
        screen.resetGridEffect();
        for(Connection c: paths)
        {
            screen.putSelectedShadow(c.x1, c.y1);
            screen.putSelectedShadow(c.x2, c.y2);
            screen.highlightPath(c.x1, c.y1, c.x2, c.y2);
        }
    }

    private boolean highlightByLetter(char letter)
    {
        boolean hasLetter = false;
        for(int i = 0; i < screen.getNumberOfRows(); i++)
            for(int j = 0; j < screen.getNumberOfColumns(); j++)
                if(data.getCharGrid()[i][j] == letter)
                {
                    screen.putSelectedShadow(i, j);
                    hasLetter = true;
                }
        return hasLetter;
    }

    public void setKeyTypeHandler()
    {
        screen.setOnKeyTyped(event -> {
            if(isPaused || isEnded)
                return;

            if(event.getCharacter().matches("\\p{Alnum}"))
            {
                String letter = event.getCharacter().toUpperCase();
                String guessing = screen.getGuessingWord().replaceAll(" ", "");
                if(guessing.isEmpty())
                {
                    if(highlightByLetter(letter.charAt(0)))
                        updateGuessingWord(letter);
                    return;
                }

                ArrayList<Connection> paths = data.checkValidPaths(guessing + letter);
                if(paths.isEmpty())
                {
                    screen.updateGuessingWord("");
                    screen.resetGridEffect();
                    data.clearVisitedFlags();
                }
                else
                {
                    updateGuessingWord(letter);
                    highlightPaths(paths);
                }
            }
            else if(event.getCharacter().equals("\r"))
                 evaluateGuessingWord();
        });
    }


    class SelectByMouseDragHandler implements EventHandler<MouseEvent> {
        private String letter;
        private int x;
        private int y;

        public SelectByMouseDragHandler(String letter, int x, int y)
        {
            this.letter = letter;
            this.x = x;
            this.y = y;
        }

        @Override
        public void handle(MouseEvent event) {
            int prevX = data.getPointerX();
            int prevY = data.getPointerY();
            if(data.connect(x, y))
            {
                updateGuessingWord(letter);
                screen.putSelectedShadow(x, y);
                if(prevX != -1 && prevY != -1)
                    screen.highlightPath(prevX, prevY, x, y);
            }

        }
    }
}
