package controller;

import data.GameMode;
import data.UserData;
import gui.LevelSelection;
import gui.Workspace;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * @author Hsiang-Ju Lai
 */
public class LevelSelectionController {

    Workspace workspace;
    LevelSelection screen;
    UserData user;
    GameMode mode;

    public LevelSelectionController(Workspace workspace, LevelSelection screen, GameMode mode, UserData user)
    {
        this.workspace = workspace;
        this.screen = screen;
        this.user = user;
        this.mode = mode;
    }

    public void returnHomeScreen(){workspace.toHomeScreen();}

    public void startGame(int level)
    {
        workspace.toGamePlay(level, user);
    }

    public void updatePlayableLevels()
    {
        screen.updateLevelSelectable(user.levelsCleared(mode.toString()));
    }

    public void profileSetting()
    {
        workspace.toProfileSettings(true, user);
    }

    public GameMode getMode() {return mode;}

    public String getUsername(){return user.getName();}

    public void setGridHandler(TextField t)
    {
        t.setOnMouseClicked(new LevelSelectHandler(Integer.parseInt(t.getText())));
    }

    class LevelSelectHandler implements EventHandler<MouseEvent>{
        private int level;

        public LevelSelectHandler(int level){this.level = level;}

        @Override
        public void handle(MouseEvent event) {
            startGame(level);
        }
    }
}
