package controller;

import data.GameMode;
import data.UserData;
import files.AccountList;
import gui.LevelSelection;
import gui.ProfileSettings;
import gui.Workspace;
import javafx.scene.control.TextArea;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;

import static buzzword.Properties.*;

/**
 * @author Hsiang-Ju Lai
 */
public class ProfileSettingsController {

    Workspace workspace;
    private AccountList accounts;
    private UserData user;
    private ProfileSettings screen;

    private boolean isLoggedIn;
    private boolean isEditing;

    public ProfileSettingsController(Workspace workspace, ProfileSettings screen, boolean isLoggedIn, UserData user)
    {
        accounts = AccountList.getSingleton();
        this.screen = screen;
        this.workspace = workspace;
        this.isLoggedIn = isLoggedIn;
        this.user = user;
        isEditing = !isLoggedIn;
    }

    public String getUsername(){return user.getName();}

    public void editProfile()
    {
        isEditing = true;
        screen.updatedTextFields(isLoggedIn, isEditing);
        screen.updateHCBPanes();
    }

    public void endAndSave()
    {
        PropertyManager pm = PropertyManager.getManager();
        if(isLoggedIn())
        {
            accounts.changePassword(getUsername(), screen.getPassword());
            AppMessageDialogSingleton.getSingleton().show("", pm.getPropertyValue(PASSWORD_CHANGED_MESSAGE));
        }
        else
        {
            boolean valid = true;
            String username = screen.getUsername();

            if(!username.matches("\\p{Alnum}+?"))
            {
                screen.showUsernameHint(pm.getPropertyValue(INVALID_USERNAME));
                valid = false;
            }
            else if (accounts.checkUser(username))
            {
                screen.showUsernameHint(pm.getPropertyValue(USERNAME_EXISTED));
                valid = false;
            }
            else
                screen.showUsernameHint("");

            if(!screen.passwordsMatch())
            {
                screen.showConfirmHint(pm.getPropertyValue(PASSWORDS_NOT_MATCH));
                valid = false;
            }
            else
                screen.showConfirmHint("");

            if(!valid)
                return;

            accounts.addUser(username, screen.getPassword());
            AppMessageDialogSingleton.getSingleton().show("", pm.getPropertyValue(ACCOUNT_CREATED_MESSAGE));
        }

        end();
    }

    public void putUserRecords(TextArea ta)
    {
        PropertyManager pm = PropertyManager.getManager();
        String level = pm.getPropertyValue(LEVEL);
        String records = pm.getPropertyValue(PERSONAL_BEST) + "\n\t\t";
        for(int i = 1; i <= LevelSelection.NUMBER_OF_LEVELS; i++)
            records += "\t" + level + i;
        for(GameMode mode: GameMode.values())
            records += "\n" + user.bestScoresByMode(mode);

        ta.setText(records);
    }

    public void end(){workspace.returnFromProfile();}

    public boolean isLoggedIn() {return isLoggedIn;}

    public boolean isEditing() {return isEditing;}
}
