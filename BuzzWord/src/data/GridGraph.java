package data;

/**
 * @author Hsiang-Ju Lai
 */
public class GridGraph {

    CharNode[][] nodes;

    public GridGraph(GameData data)
    {
        nodes = new CharNode[GameData.NROWS][GameData.NCOLS];
        char[][] letters = data.getCharGrid();
        for(int i = 0; i < GameData.NROWS; i++)
            for(int j = 0; j < GameData.NCOLS; j++)
                nodes[i][j] = new CharNode(letters[i][j], i, j, data);

        buildConnections();
    }


    private void buildConnections()
    {
        for(int i = 0; i < GameData.NROWS; i++)
            for(int j = 0; j < GameData.NCOLS; j++)
            {
                //top
                if(i - 1 >= 0)
                    nodes[i][j].addConnection(nodes[i-1][j]);

                //top-right
                if(i - 1 >= 0 && j + 1 < GameData.NCOLS)
                    nodes[i][j].addConnection(nodes[i-1][j+1]);

                //right
                if(j + 1 < GameData.NCOLS)
                    nodes[i][j].addConnection(nodes[i][j+1]);

                //bottom-right
                if(i + 1 < GameData.NROWS && j + 1 < GameData.NCOLS)
                    nodes[i][j].addConnection(nodes[i+1][j+1]);

                //bottom
                if(i + 1 < GameData.NROWS)
                    nodes[i][j].addConnection(nodes[i+1][j]);

                //bottom-left
                if(i + 1 < GameData.NROWS && j - 1 >= 0)
                    nodes[i][j].addConnection(nodes[i+1][j-1]);

                //left
                if(j - 1 >= 0)
                    nodes[i][j].addConnection(nodes[i][j-1]);

                //top-left
                if(i - 1 >= 0 && j - 1 >= 0)
                    nodes[i][j].addConnection(nodes[i-1][j-1]);
            }
    }

    public void checkPaths(String word)
    {
        for(int i = 0; i < GameData.NROWS; i++)
            for(int j = 0; j < GameData.NCOLS; j++)
                nodes[i][j].checkPath(word);
    }



    public void buildWords()
    {
        for(int i = 0; i < GameData.NROWS; i++)
            for(int j = 0; j < GameData.NCOLS; j++)
                nodes[i][j].buildWord("");
    }
}
