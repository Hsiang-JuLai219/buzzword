package data;

import propertymanager.PropertyManager;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import static buzzword.Properties.WORDS_LISTS_PATH;

/**
 * @author Hsiang-Ju Lai
 */
public class WordsLists {
    private ArrayList<String> dictionary, names, places, science;

    public WordsLists()
    {
        dictionary = new ArrayList<>();
        names = new ArrayList<>();
        places = new ArrayList<>();
        science = new ArrayList<>();
    }

    public boolean load()
    {
        for (GameMode mode: GameMode.values())
            if(!loadList(mode))
                return false;
        return true;
    }

    private ArrayList<String> chooseList(GameMode mode)
    {
        switch (mode)
        {
            case DICTIONARY: return dictionary;
            case NAMES: return names;
            case PLACES: return places;
            case SCIENCE: return science;
            default: return null;
        }
    }

    private boolean loadList(GameMode mode)
    {
        PropertyManager pm = PropertyManager.getManager();
        String path = pm.getPropertyValue(WORDS_LISTS_PATH) + pm.getPropertyValue(mode);
        ArrayList<String> theList = chooseList(mode);

        try
        {
            String file = getClass().getClassLoader().getResource(path).getFile();
            FileInputStream input = new FileInputStream(file);
            BufferedInputStream bis = new BufferedInputStream(input);
            DataInputStream dis = new DataInputStream(bis);
            int size = dis.readInt();
            theList.ensureCapacity(size);
            for(int i = 0; i < size; i++)
                theList.add(dis.readUTF());

            dis.close();
            bis.close();
            input.close();
        }
        catch (IOException e) {return false;}

        return true;
    }

//    private void updateDictionary() throws IOException
//    {
//        Scanner sc = new Scanner(input);
//        while(sc.hasNextLine())
//            add(sc.nextLine().toUpperCase());
//        saveList();
//
//        String path = getClass().getClassLoader().getResource("word/Science").getFile();
//        FileOutputStream fos = new FileOutputStream(path);
//        BufferedOutputStream bos = new BufferedOutputStream(fos);
//        DataOutputStream dos = new DataOutputStream(bos);
//        dos.writeInt(size());
//        for(int i = 0; i < size(); i++)
//            dos.writeUTF(get(i));
//        dos.flush();
//        bos.flush();
//        fos.flush();
//        dos.close();
//        bos.close();
//        fos.close();
//    }

    /**
     *
     * @param candidate
     * @return first entry -- isPrefix
     *         second entry -- isWord
     */
    public boolean[] evaluate(String candidate, GameMode mode)
    {
        ArrayList<String> theList = chooseList(mode);

        boolean[] result = new boolean[2];
        int pos = Collections.binarySearch(theList, candidate) ;
        if (pos >= 0)
        {
            // The prefix is a word. Check the following word, because we are looking
            result[1] = theList.get(pos).length() >= 3; // isWord
            // for words that are longer than the prefix
            if (pos + 1 < theList.size())
                result[0] = theList.get(pos+1).startsWith(candidate);
            else
                result[0] = false;
        }
        else
        {
            result[1] = false; //is NOT a word
            pos = -(pos+1);
            // The prefix is not a word. Check where it would be inserted and get the next word.
            // If it starts with prefix, return true.
            if (pos == theList.size())
                result[0] = false;
            else
                result[0] = theList.get(pos).startsWith(candidate);
        }

        return result;
    }
}
