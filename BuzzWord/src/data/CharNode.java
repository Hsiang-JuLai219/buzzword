package data;

import java.util.ArrayList;

/**
 * @author Hsiang-Ju Lai
 */
public class CharNode {

    private int x, y;

    private char letter;

    private boolean isVisited;

    private ArrayList<CharNode> connections;

    private GameData gameData;

    public CharNode(char letter, int x, int y, GameData gameData)
    {
        this.letter = letter;
        this.x = x;
        this.y = y;
        connections = new ArrayList<>(8);
        isVisited = false;
        this.gameData = gameData;
    }

    public char getLetter() {return letter;}

    public void setLetter(char letter) {this.letter = letter;}

    public void addConnection(CharNode to){connections.add(to);}

    public void buildWord(String prefix)
    {
        isVisited = true;
        boolean[] result = gameData.wordsLists.evaluate(prefix + letter, gameData.getMode());
        if(result[1])
            gameData.words.add(prefix + letter);

        if(!result[0])
        {
            isVisited = false;
            return;
        }
        else
        {
            for(CharNode next: connections)
                if(!next.isVisited)
                {
                    next.buildWord(prefix + letter);
                    next.isVisited = false;
                }
            isVisited = false;
        }

    }

    public boolean checkPath(String letterSequence)
    {
        if(letter != letterSequence.charAt(0))
            return false;
        else if(letterSequence.length() == 1)
            return true;
        else
        {
            isVisited = true;
            boolean validPath = false;
            for(CharNode next: connections)
                if(!next.isVisited && next.checkPath(letterSequence.substring(1)))
                {
                    gameData.paths.add(new Connection(next.x, next.y, x, y));
                    validPath = true;
                }
            isVisited = false;
            return validPath;
        }
    }


    public boolean isVisited() {return isVisited;}

    public void setVisited(boolean visited) {isVisited = visited;}

    public boolean isConnectedTo(CharNode node) {return connections.contains(node);}
}
