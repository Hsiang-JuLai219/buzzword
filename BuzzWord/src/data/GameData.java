package data;

import components.AppDataComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;



/**
 * @author Hsiang-Ju Lai
 */
public class GameData implements AppDataComponent{

    public static final int GUESSING_TIME = 100;
    private static final int MINIMUM_WORDS = 10;
    private static final int NUMBER_OF_CHARS = 16;
    static final int NROWS = 4;
    static final int NCOLS = 4;

    private GameMode mode;
    private int level;

    private int targetScore;
    private int currentScore;
    private char[][] charGrids;
    private GridGraph gridGraph;
    private ObservableMap<String, String> guessed = FXCollections.observableHashMap();
    private boolean isDictionaryReady;
    WordsLists wordsLists;
    Set<String> words;
    ArrayList<Connection> paths;

    private int pointerX;
    private int pointerY;

    public GameData()
    {
        isDictionaryReady = false;
        wordsLists = new WordsLists();

        new Thread(()-> {
            isDictionaryReady = wordsLists.load();
            if(!isDictionaryReady)
            {
                System.out.println("Fail to load the word list!");
                System.exit(1);
            }
        }).start();

        words = new HashSet<>();
    }

    private void setPointer(int x, int y)
    {
        pointerX = x;
        pointerY = y;
    }

    //just for testing
    public void setCharGrids(String s)
    {
        charGrids = new char[4][4];
        for(int i = 0; i < 16; i++)
            charGrids[i/4][i%4] = s.charAt(i);
    }


    public void setTargetWords()
    {
        while(!isDictionaryReady)
            try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}

        setPointer(-1, -1);
        do
        {
            reset();
            generateCharGrid();
            gridGraph = new GridGraph(this);
            gridGraph.buildWords();
        }
        while (words.size() < MINIMUM_WORDS);

        for(String word: words)
            targetScore += computeWordScore(word);

        targetScore = targetScore * level / 10 + 5 - (targetScore * level / 10) % 5;
        //for testing
        words.forEach(System.out::println);
    }

    private void generateCharGrid()
    {
        charGrids = new char[NROWS][NCOLS];
        for(int i = 0; i < NUMBER_OF_CHARS; i++)
            charGrids[i/NROWS][i%NCOLS] = (char)(Math.random() * ('Z' - 'A' + 1) + 'A');
    }


    private void updateScore(int points) {currentScore += points;}

    public char[][] getCharGrid()
    {
        return charGrids;
    }

    public GameMode getMode() {return mode;}

    public void setMode(GameMode mode) {this.mode = mode;}

    public int getTargetScore() {return targetScore;}

    public Set<String> getWords() {return words;}

    public int getLevel() {return level;}

    public void setLevel(int level) {this.level = level;}

    private static int computeWordScore(String word){return (word.length() - 2) * 5;}

    public boolean checkWord(String word)
    {
        if(!words.contains(word) || guessed.containsKey(word))
            return false;
        else
            updateScore(computeWordScore(word));
        return true;
    }

    public void showAnswers()
    {
        for(String word: words)
            if(!guessed.containsKey(word))
                guessed.put(word, "--");
    }

    public void addGuessed(String word) {guessed.put(word, Integer.toString(computeWordScore(word)));}

    public ObservableMap<String, String> getGuessed() {return guessed;}

    public int getCurrentScore(){return currentScore;}

    public boolean connect(int x, int y)
    {
        if((pointerX == -1 && pointerY == -1) || (!gridGraph.nodes[x][y].isVisited()
                && gridGraph.nodes[pointerX][pointerY].isConnectedTo(gridGraph.nodes[x][y])))
        {
            setPointer(x, y);
            gridGraph.nodes[pointerX][pointerY].setVisited(true);
            return true;
        }
        else
            return false;
    }

    public ArrayList<Connection> checkValidPaths(String word)
    {
        paths = new ArrayList<>();
        gridGraph.checkPaths(word);
        return paths;
    }



    public void clearVisitedFlags()
    {
        setPointer(-1, -1);
        for(int i = 0; i < GameData.NROWS; i++)
            for(int j = 0; j < GameData.NCOLS; j++)
                gridGraph.nodes[i][j].setVisited(false);
    }

    public int getPointerX(){return pointerX;}
    public int getPointerY(){return pointerY;}

    @Override
    public void reset()
    {
        currentScore = 0;
        targetScore = 0;
        words.clear();
        guessed.clear();
        guessed.put("", "");
    }
}
