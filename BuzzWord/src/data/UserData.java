package data;

import components.AppDataComponent;
import gui.LevelSelection;
import propertymanager.PropertyManager;

import java.util.HashMap;

/**
 * @author Hsiang-Ju Lai
 */
public class UserData implements AppDataComponent{


    private String name;

    private HashMap<String, int[]> records;

    public UserData(String name)
    {
        this.name = name;
        records = new HashMap<>();
    }

    public void initRecords()
    {
        int[] temp = new int[LevelSelection.NUMBER_OF_LEVELS];
        temp[0] = 0;
        for(int i = 1; i < LevelSelection.NUMBER_OF_LEVELS; i++)
            temp[i] = -1;
        for(GameMode mode : GameMode.values())
            records.put(mode.toString(), temp.clone());
    }

    public String bestScoresByMode(GameMode mode)
    {
        PropertyManager pm = PropertyManager.getManager();
        int[] scores = records.get(mode.toString());
        String info = pm.getPropertyValue(mode) + ":";
        for(int i: scores)
            info += "\t\t" + (i == -1 ? "N/A" : i);
        return info;
    }

    /**
     *
     * @param mode
     * @param level
     * @param score
     * @return isPersonalBest
     */
    public boolean updateRecord(String mode, int level, int score)
    {
        if(score > records.get(mode)[level-1])
        {
            records.get(mode)[level-1] = score;
            return true;
        }

        return false;
    }

    public void clearLevel(String mode, int level)
    {
        if(level < LevelSelection.NUMBER_OF_LEVELS && records.get(mode)[level] == -1)
            records.get(mode)[level] = 0;
    }

    public void setRecords(HashMap<String, int[]> records){this.records = records;}

    public HashMap<String, int[]> getRecords(){return records;}

    public int levelsCleared(String mode)
    {
        int[] scores = records.get(mode);
        for(int i = 0; i < LevelSelection.NUMBER_OF_LEVELS; i++)
            if(scores[i] == -1)
                return i;
        return LevelSelection.NUMBER_OF_LEVELS;
    }

    public String getName() {return name;}

    @Override
    public void reset() {}
}
