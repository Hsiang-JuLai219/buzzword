package data;

/**
 * @author Hsiang-Ju Lai
 */
public enum GameMode {
    DICTIONARY,
    PLACES,
    SCIENCE,
    NAMES
}
