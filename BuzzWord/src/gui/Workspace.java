package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.GamePlayController;
import controller.HomeController;
import controller.LevelSelectionController;
import controller.ProfileSettingsController;
import data.GameData;
import data.GameMode;
import data.UserData;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import ui.AppGUI;

/**
 * @author Hsiang-Ju Lai
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate      app;    // the actual application
    AppGUI           gui;    // the GUI inside which the application sits
    GameData         data;

    StackPane        screens;
    HomeScreen       homeScreen;
    LevelSelection   selectionScreen;
    GamePlay         gameScreen;
    Help             helpScreen;
    ProfileSettings  profileSettings;

    public Workspace(AppTemplate app)
    {
        this.app = app;
        this.gui = app.getGUI();
        data = (GameData)app.getDataComponent();
        initLoginPopUp();

        homeScreen = new HomeScreen();
        selectionScreen = new LevelSelection();
        gameScreen = new GamePlay();
        profileSettings = new ProfileSettings();
        helpScreen = new Help(this);

        initHomeScreen();
        screens = new StackPane();
        screens.setAlignment(Pos.TOP_CENTER);
        screens.getChildren().setAll(homeScreen);
        workspace = screens;
        activateWorkspace(gui.getAppPane());
        setupKeyShortCuts();
    }

    @Override
    public void initStyle() {}

    @Override
    public void reloadWorkspace() {}

    private void initHomeScreen()
    {
        homeScreen.controller = new HomeController(homeScreen, app.getFileComponent(), this);
        homeScreen.initLayout();
    }

    public void toHomeScreen(){screens.getChildren().setAll(homeScreen);}

    public void toLevelSelection(GameMode mode, UserData user)
    {
        data.setMode(mode);
        selectionScreen.controller = new LevelSelectionController(this, selectionScreen, mode, user);
        selectionScreen.initLayout();
        screens.getChildren().setAll(selectionScreen);
    }

    public void toGamePlay(int level, UserData user)
    {
        data.reset();
        data.setLevel(level);
        if(gameScreen.controller == null)
            gameScreen.controller = new GamePlayController(this, gameScreen, data, user);
        else
            gameScreen.controller.reset(user);
        if(!gameScreen.initialized)
            gameScreen.initLayout();
        gameScreen.reset();
        gameScreen.controller.startPlaying();
        screens.getChildren().setAll(gameScreen);
    }

    public void toProfileSettings(boolean isLoggedIn, UserData user)
    {
        profileSettings.controller = new ProfileSettingsController(this, profileSettings, isLoggedIn, user);
        profileSettings.initLayout();
        screens.getChildren().add(profileSettings);
    }

    public void toHelpScreen()
    {
        if(!helpScreen.initialized)
            helpScreen.initLayout();
        screens.getChildren().setAll(helpScreen);
    }

    public void returnFromProfile()
    {
        screens.getChildren().remove(profileSettings);
        if(screens.getChildren().get(0) == gameScreen)
            gameScreen.controller.pauseResumeGame();
    }

    //return true if state has been changed.
    public boolean pauseGamePlay(boolean isPaused)
    {
        if(screens.getChildren().get(0) == gameScreen && isPaused^gameScreen.controller.isPaused())
        {
            gameScreen.controller.setPaused(isPaused);
            return true;
        }
        return false;
    }

    private void setupKeyShortCuts()
    {
        Scene scene = gui.getPrimaryScene();
        scene.setOnKeyPressed(event ->
        {
            Node screen = screens.getChildren().get(0);
            if(event.isControlDown())
                switch (event.getCode())
                {
                    case L:
                        if(screen == homeScreen)
                            homeScreen.login.fire();
                        break;
                    case P:
                        if(screen == homeScreen && event.isShiftDown() && !homeScreen.controller.isLoggedIn())
                            homeScreen.createProfile.fire();
                        else if(screen == homeScreen && homeScreen.controller.isLoggedIn())
                            homeScreen.startGame.fire();
                        break;
                    case Q:
                        app.getFileComponent().exitApplication();
                        break;
                    case H:
                        if(screen == selectionScreen || screen == helpScreen)
                            toHomeScreen();
                        else if(screen == profileSettings)
                            returnFromProfile();
                        else if(screen == gameScreen)
                            gameScreen.home.fire();
                        break;
                    case R:
                        if(screen == gameScreen)
                            gameScreen.replay.fire();
                        break;
                    case N:
                        if(screen == gameScreen && gameScreen.nextLevel.isVisible())
                            gameScreen.nextLevel.fire();
                        break;
                }
        });
    }

    private void initLoginPopUp() {LoginPopUp.getSingleton().init(gui.getWindow());}

    public AppTemplate getApp(){return app;}


}
