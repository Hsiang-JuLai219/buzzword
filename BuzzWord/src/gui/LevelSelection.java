package gui;

import controller.LevelSelectionController;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import propertymanager.PropertyManager;

import static buzzword.Properties.*;

/**
 * @author Hsiang-Ju Lai
 */
public class LevelSelection extends BasicScreen {

    public static final int NUMBER_OF_LEVELS = 8;
    LevelSelectionController controller;
    //heading pane
    Label gameMode;

    //control pane
    Button user;
    Button home;

    //display pane

    //bottom pane

    //for girds

    public LevelSelection()
    {
        super();
        rows = 2;
    }

    @Override
    public void initLayout() {
        PropertyManager pm = PropertyManager.getManager();

        initLetters();
        initGrid(true);

        gameMode = new Label(pm.getPropertyValue(controller.getMode()));
        user = new Button(controller.getUsername());
        home = new Button(pm.getPropertyValue(HOME));

        gameMode.setId("game-mode");
        user.setTooltip(new Tooltip(pm.getPropertyValue(USER_TOOLTIP)));
        home.setTooltip(new Tooltip(pm.getPropertyValue(HOME_TOOLTIP)));

        headingPane.getChildren().setAll(gameMode);
        controlPane.getChildren().setAll(user, home);

        controller.updatePlayableLevels();

        setupHandlers();
    }

    @Override
    public void setupHandlers()
    {
        home.setOnAction(event -> controller.returnHomeScreen());
        user.setOnAction(event -> controller.profileSetting());
        for(int i = 0; i < rows; i++)
            for(int j = 0; j < GRID_COLUMNS; j++)
                controller.setGridHandler(grid[i][j]);
    }

    public void updateLevelSelectable(int level)
    {
        for(int i = level; i < NUMBER_OF_LEVELS; i++)
            grid[i/4][i%4].setDisable(true);
    }



    private void initLetters()
    {
        letters = new char[rows][GRID_COLUMNS];
        for(int i = 0; i < rows; i++)
            for(int j = 0; j < GRID_COLUMNS; j++)
                letters[i][j] = (char)(i*4 + j + '1');
    }
}
