package gui;

import controller.GamePlayController;
import data.GameData;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import propertymanager.PropertyManager;

import static buzzword.Properties.*;

/**
 * @author Hsiang-Ju Lai
 */
public class GamePlay extends BasicScreen{

    GamePlayController controller;
    boolean initialized;
    //heading pane
    Label gameMode;
    Label timeLeft;

    //control pane
    Button user;
    Button home;
    Button replay;
    Button nextLevel;

    //display pane
    Label guessingWord;
    TableView<ObservableMap.Entry<String, String>> guessedWords;
    Label totalScore;

    //bottom pane
    Label level;
    Button pauseResume;    //set shape
    VBox bottomDisplay;
    Label targetScore;


    //for girds
    Line[][] horizontalLines;
    Line[][] verticleLines;
    Line[][] slashes;
    Line[][] backSlashes;
    Pane lines;
    StackPane lineGrid;

    //shapes
    private Polygon triangle;
    private Rectangle square;

    //timer label
    String timeRemaining;
    String seconds;
    int counter;

    //shadow
    private DropShadow selectedShadow;

    public GamePlay()
    {
        super();
        initialized = false;
    }

    @Override
    public void initLayout()
    {
        PropertyManager pm = PropertyManager.getManager();

        //--------------------------------------------------//
        user = new Button();
        home = new Button(pm.getPropertyValue(HOME));
        replay = new Button(pm.getPropertyValue(REPLAY));
        nextLevel = new Button(pm.getPropertyValue(NEXT_LEVEL));
        user.setTooltip(new Tooltip(pm.getPropertyValue(USER_TOOLTIP)));
        home.setTooltip(new Tooltip(pm.getPropertyValue(HOME_TOOLTIP)));
        replay.setTooltip(new Tooltip(pm.getPropertyValue(REPLAY_TOOLTIP)));
        nextLevel.setTooltip(new Tooltip(pm.getPropertyValue(NEXT_LEVEL_TOOLTIP)));
        updateControlPane(false);
        //--------------------------------------------------//
        guessingWord = new Label();
        guessedWords = new TableView<>();
        totalScore = new Label();
        VBox scoreBoard = new VBox();
        scoreBoard.setAlignment(Pos.TOP_RIGHT);
        guessingWord.setId("guessing-word");
        guessingWord.setTooltip(new Tooltip(pm.getPropertyValue(GUESSING_TOOLTIP)));
        guessedWords.setTooltip(new Tooltip(pm.getPropertyValue(GUESSED_TOOLTIP)));
        scoreBoard.getChildren().setAll(guessedWords, totalScore);
        initScoreBoard();
        displayPane.setPadding(new Insets(0, 20, 0, 0));
        displayPane.setAlignment(Pos.TOP_RIGHT);
        displayPane.getChildren().setAll(guessingWord, scoreBoard);
        //--------------------------------------------------//
        horizontalLines = new Line[rows][GRID_COLUMNS-1];
        verticleLines = new Line[rows - 1][GRID_COLUMNS];
        slashes = new Line[rows - 1][GRID_COLUMNS-1];
        backSlashes = new Line[rows - 1][GRID_COLUMNS-1];
        initLines();
        //--------------------------------------------------//
        level = new Label();
        pauseResume = new Button();
        targetScore = new Label();
        pauseResume.setTooltip(new Tooltip(pm.getPropertyValue(PAUSE_TOOLTIP)));
        level.setId("level");
        pauseResume.setId("pause-resume");
        targetScore.setId("target");
        initShapes();
        updatePauseResumeButton(false);
        bottomDisplay = new VBox(25);
        bottomDisplay.getChildren().setAll(level, pauseResume);
        bottomDisplay.setAlignment(Pos.CENTER);
        bottomDisplay.setMinSize(200, 100);
        bottomDisplay.setMaxSize(200, 100);
        HBox l = new HBox();
        HBox r = new HBox();
        l.setMinSize(360, 100);
        l.setMaxSize(360, 100);
        r.setMinSize(360, 100);
        r.setMaxSize(360, 100);
        r.getChildren().setAll(targetScore);
        r.setAlignment(Pos.TOP_RIGHT);
        bottomPane.setMinSize(1000, 200);
        bottomPane.setMaxSize(1000, 200);
        bottomPane.setAlignment(Pos.TOP_CENTER);
        bottomPane.getChildren().setAll(l, bottomDisplay, r);
        //--------------------------------------------------//
        gameMode = new Label();
        timeLeft = new Label();
        timeRemaining = pm.getPropertyValue(TIME_REMAINING);
        seconds = pm.getPropertyValue(SECONDS);
        gameMode.setId("game-mode");
        timeLeft.setId("time-remaining");
        HBox left = new HBox();
        left.setMinSize(400, 150);
        left.setMaxSize(400, 150);
        HBox right = new HBox();
        right.setMinSize(400, 150);
        right.setMaxSize(400, 150);
        HBox center = new HBox();
        center.setPrefSize(300, 150);

        center.getChildren().setAll(gameMode);
        center.setAlignment(Pos.CENTER);

        right.getChildren().setAll(timeLeft);
        right.setAlignment(Pos.BOTTOM_RIGHT);
        right.setPadding(new Insets(0, 15, 10, 0));

        headingPane.setAlignment(Pos.CENTER);
        headingPane.getChildren().setAll(left, center, right);
        setupHandlers();
        initShadowEffect();

        initialized = true;
    }

    public void reset()
    {
        guessingWord.setText("");
        setTimeLeft(GameData.GUESSING_TIME);
        updateScore();
        updatePauseResumeButton(false);
        updateControlPane(false);
        setGridVisible(true);
        PropertyManager pm = PropertyManager.getManager();
        user.setText(controller.getUsername());
        level.setText(pm.getPropertyValue(LEVEL) + " " + controller.getCurrentLevel());
        gameMode.setText(pm.getPropertyValue(controller.getMode()));
        Platform.runLater(this::requestFocus);
    }

    public void initLetterGrid()
    {
        letters = controller.getLetterGrid();
        initGrid(true);
        loadTargetScore();

        controller.setupGridHandler(grid);
    }

    public int getNumberOfRows(){return rows;}
    public int getNumberOfColumns(){return GRID_COLUMNS;}

    private void setTimeLeft(int secs)
    {
        if(secs < 0)
            return;
        counter = secs;
        timeLeft.setText(timeRemaining + counter + seconds);
    }

    public boolean countDown()
    {
        setTimeLeft(counter - 1);
        return counter == 0;
    }

    @Override
    public void setupHandlers()
    {
        home.setOnAction(event -> controller.returnToHomeScreen());
        user.setOnAction(event -> controller.profileSettings());
        pauseResume.setOnAction(event -> controller.pauseResumeGame());
        replay.setOnAction(event -> controller.replay());
        nextLevel.setOnAction(event -> controller.startNextLevel());
        addEventFilter(MouseEvent.DRAG_DETECTED , event -> startFullDrag());
        setOnMouseDragReleased(event -> controller.evaluateGuessingWord());

        setFocusTraversable(true);
        Platform.runLater(this::requestFocus);
        controller.setKeyTypeHandler();

    }

    private void loadTargetScore()
    {
        targetScore.setText(PropertyManager.getManager().getPropertyValue(TARGET) + "\n" + controller.getTargetScore());
    }

    public void updateControlPane(boolean isCleared)
    {
        controlPane.getChildren().setAll(user, home, replay);
        if(isCleared)
            controlPane.getChildren().add(nextLevel);
    }

    public void setGridVisible(boolean isVisible)
    {
        textGrid.setVisible(isVisible);
        lines.setVisible(isVisible);
    }

    public void updatePauseResumeButton(boolean isPaused)
    {
        if(isPaused)
            pauseResume.setShape(square);
        else
            pauseResume.setShape(triangle);
    }

    public void disableResumeButton(boolean isDisabled){pauseResume.setDisable(isDisabled);}

    private void initShapes()
    {
        square = new Rectangle(50, 50);
        triangle = new Polygon();
        triangle.getPoints().addAll(0.0, 0.0,
                50.0, 25.0,
                0.0, 50.0);
    }

    private void initLines()
    {
        final int RADIUS = 50;
        final int VGAP = 40 + RADIUS;
        final int HGAP = 50 + RADIUS;

        for(int i = 0; i < rows; i++)
            for(int j = 0; j < GRID_COLUMNS-1; j++)
            {
                horizontalLines[i][j] = new Line(125+HGAP*j, 25+VGAP*i, 175+HGAP*j, 25+VGAP*i);
                horizontalLines[i][j].setStrokeWidth(2.0);
            }

        for(int i = 0; i < rows-1; i++)
            for(int j = 0; j < GRID_COLUMNS; j++)
            {
                verticleLines[i][j] = new Line(100+HGAP*j, 50+VGAP*i, 100+HGAP*j, 90+VGAP*i);
                verticleLines[i][j].setStrokeWidth(2.0);
            }

        for(int i = 0; i < rows-1; i++)
            for(int j = 0; j < GRID_COLUMNS-1; j++)
            {
                slashes[i][j] = new Line(118+HGAP*j, 40+VGAP*i, 182+HGAP*j, 100+VGAP*i);
                slashes[i][j].setStrokeWidth(1.5);
            }

        for(int i = 0; i < rows-1; i++)
            for(int j = 0; j < GRID_COLUMNS-1; j++)
            {
                backSlashes[i][j] = new Line(182+HGAP*j, 41+VGAP*i, 118+HGAP*j, 98+VGAP*i);
                backSlashes[i][j].setStrokeWidth(1.5);
            }

        lineGrid = new StackPane();
        lines = new Pane();
        lines.setBackground(Background.EMPTY);

        for(int i = 0; i < rows; i++)
            lines.getChildren().addAll(horizontalLines[i]);

        for(int i = 0; i < rows-1; i++)
            lines.getChildren().addAll(verticleLines[i]);

        for(int i = 0; i < rows-1; i++)
            lines.getChildren().addAll(slashes[i]);

        for(int i = 0; i < rows-1; i++)
            lines.getChildren().addAll(backSlashes[i]);

        lineGrid.getChildren().setAll(lines, textGrid);
        this.setCenter(lineGrid);
    }

    private void initScoreBoard()
    {
        ObservableMap<String, String> guessed = controller.getGuessed();
        TableColumn<ObservableMap.Entry<String, String>, String> words = new TableColumn<>();
        TableColumn<ObservableMap.Entry<String, String>, String> scores = new TableColumn<>();

        words.prefWidthProperty().bind(guessedWords.widthProperty().multiply(0.69));
        scores.prefWidthProperty().bind(guessedWords.widthProperty().multiply(0.30));
        words.setResizable(false);
        scores.setResizable(false);


        words.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getKey()));
        scores.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getValue()));

        words.setComparator(String::compareTo);
        words.setSortType(TableColumn.SortType.ASCENDING);

        guessed.addListener((MapChangeListener<? super String, ? super String>) change ->
        {
            guessedWords.setItems(FXCollections.observableArrayList(guessed.entrySet()));
            if(guessed.size() == 2 && guessed.containsKey(""))
                guessed.remove("");
            guessedWords.getSortOrder().add(words);
            guessedWords.sort();
        });

        guessedWords.getColumns().setAll(words, scores);

        guessed.put("", ""); //avoid placeholder of table view

        totalScore.setId("total-score");
        totalScore.setPadding(new Insets(0, 0, 0, 20));

        updateScore();
    }

    public void putSelectedShadow(int x, int y)
    {
        if(x >= rows || y >= GRID_COLUMNS)
            return;

        grid[x][y].setEffect(selectedShadow);
    }

    public void highlightPath(int x1, int y1, int x2, int y2)
    {
        if (x1 == x2)    //Horizontal lines
            horizontalLines[x1][(y1 + y2) / 2].setEffect(selectedShadow);
        else if (y1 == y2)
            verticleLines[(x1 + x2) / 2][y1].setEffect(selectedShadow);
        else if ((x1-x2)*(y1-y2) > 0)
            slashes[(x1+x2)/2][(y1+y2)/2].setEffect(selectedShadow);
        else
            backSlashes[(x1+x2)/2][(y1+y2)/2].setEffect(selectedShadow);
    }

    public void resetGridEffect()
    {
        for(int i = 0; i < rows; i++)
            for(int j = 0; j < GRID_COLUMNS; j++)
                grid[i][j].setEffect(shadow);

        for(int i = 0; i < rows; i++)
            for(int j = 0; j < GRID_COLUMNS-1; j++)
                horizontalLines[i][j].setEffect(null);

        for(int i = 0; i < rows-1; i++)
            for(int j = 0; j < GRID_COLUMNS; j++)
                verticleLines[i][j].setEffect(null);

        for(int i = 0; i < rows-1; i++)
            for(int j = 0; j < GRID_COLUMNS-1; j++)
            {
                backSlashes[i][j].setEffect(null);
                slashes[i][j].setEffect(null);
            }
    }

    public void updateGuessingWord(String word) {guessingWord.setText(word);}
    public String getGuessingWord(){return guessingWord.getText();}

    public void updateScore()
    {
        totalScore.setText(PropertyManager.getManager().getPropertyValue(TOTAL_SCORE)
                + "\t\t" + controller.getCurrentScore());
    }

    private void initShadowEffect()
    {
        selectedShadow = new DropShadow();
        selectedShadow.setBlurType(BlurType.THREE_PASS_BOX);
        selectedShadow.setRadius(5);
        selectedShadow.setSpread(0.5);
        selectedShadow.setColor(Color.LIGHTGREEN);
    }

    public void disableGamePlay(boolean isDisabled)
    {
        for(int i = 0; i < rows; i++)
            for(int j = 0; j < GRID_COLUMNS; j++)
                grid[i][j].setMouseTransparent(isDisabled);
    }


}
