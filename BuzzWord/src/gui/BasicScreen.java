package gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * @author Hsiang-Ju Lai
 */
public abstract class BasicScreen extends BorderPane {

    protected static final int GRID_COLUMNS = 4;
    protected int rows = 4;

    protected GridPane textGrid;
    protected VBox controlPane;
    protected VBox displayPane;
    protected HBox headingPane;
    protected HBox bottomPane;
    protected TextField[][] grid;
    protected char[][] letters;
    protected DropShadow shadow;


    public BasicScreen() {initialize();}

    private void initialize()
    {
        textGrid = new GridPane();
        controlPane = new VBox(30);
        displayPane = new VBox(10);
        headingPane = new HBox();
        bottomPane = new HBox();

        controlPane.setMinSize(250, 350);
        displayPane.setMinSize(250, 350);
        headingPane.setMinSize(1000, 150);
        bottomPane.setAlignment(Pos.CENTER);

        textGrid.setAlignment(Pos.TOP_CENTER);
        controlPane.setAlignment(Pos.TOP_LEFT);
        controlPane.setPadding(new Insets(0, 0, 0, 50));
        displayPane.setAlignment(Pos.CENTER);
        headingPane.setAlignment(Pos.CENTER);
        bottomPane.setAlignment(Pos.CENTER);

        setTop(headingPane);
        setCenter(textGrid);
        setBottom(bottomPane);
        setLeft(controlPane);
        setRight(displayPane);

    }

    private void initShadow()
    {
        shadow = new DropShadow();
        shadow.setBlurType(BlurType.THREE_PASS_BOX);
        shadow.setRadius(4);
        shadow.setSpread(0.5);
        shadow.setOffsetX(3);
        shadow.setOffsetY(3);
        shadow.setColor(Color.valueOf("2d2d2d"));
    }

    public abstract void initLayout();

    /**
     *  row and letters[][] have to be initialized
     */
    protected void initGrid(boolean isClickable)
    {
        initShadow();
        textGrid.getChildren().clear();
        textGrid.setHgap(50);
        textGrid.setVgap(40);
        grid = new TextField[rows][GRID_COLUMNS];
        for(int i = 0; i < rows; i++)
            for(int j = 0; j < GRID_COLUMNS; j++)
            {
                grid[i][j] = new TextField(Character.toString(letters[i][j]));
                grid[i][j].setEditable(false);
                grid[i][j].setMouseTransparent(!isClickable);
                grid[i][j].setEffect(shadow);
                grid[i][j].setContextMenu(new ContextMenu());
                textGrid.add(grid[i][j], j, i);
            }
    }

    public abstract void setupHandlers();

}
