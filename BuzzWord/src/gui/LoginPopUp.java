package gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import propertymanager.PropertyManager;

import java.io.File;
import java.net.URL;

import static buzzword.Properties.*;
import static settings.AppPropertyType.APP_PATH_CSS;

/**
 * @author Hsiang-Ju Lai
 */
public class LoginPopUp extends Stage{
    private static LoginPopUp singleton = null;
    VBox messagePane;
    HBox namePane;
    HBox passwordPane;
    Scene messageScene;
    Label nameLabel;
    Label passwordLabel;
    private TextField name;
    private  TextField pw;
    private String username;
    private String password;
    private boolean login = false;

    private LoginPopUp(StageStyle ss){super(ss);}

    public static LoginPopUp getSingleton() {
        if (singleton == null)
            singleton = new LoginPopUp(StageStyle.UNDECORATED);
        return singleton;
    }

    public String getUsername() {return username;}

    public String getPassword() {return password;}

    public void init(Stage owner)
    {
        PropertyManager pm = PropertyManager.getManager();
        initModality(Modality.WINDOW_MODAL); // modal => messages are blocked from reaching other windows
        initOwner(owner);

        // LABELS TO DISPLAY THE CUSTOM MESSAGE
        nameLabel = new Label(pm.getPropertyValue(USERNAME));
        passwordLabel = new Label(pm.getPropertyValue(PASSWORD));

        // TEXTFIELD
        name = new TextField();
        pw = new PasswordField();

        //Panes
        namePane = new HBox(20);
        passwordPane = new HBox(20);

        Line nline = new Line(25, 50, 270, 50);
        nline.setStrokeWidth(1.5);
        nline.setStroke(Color.WHITE);
        Line pline = new Line(25, 50, 270, 50);
        pline.setStrokeWidth(1.5);
        pline.setStroke(Color.WHITE);

        StackPane nameField = new StackPane(name, nline);
        StackPane pwField = new StackPane(pw, pline);
        nameField.setAlignment(Pos.BOTTOM_LEFT);
        pwField.setAlignment(Pos.BOTTOM_LEFT);
        namePane.getChildren().setAll(nameLabel, nameField);
        passwordPane.getChildren().setAll(passwordLabel, pwField);

        VBox messagePane = new VBox(50);
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().setAll(namePane, passwordPane);

        messagePane.setPadding(new Insets(80, 60, 80, 60));

        Scene messageScene = new Scene(messagePane);

        URL cssResource = getClass().getClassLoader().getResource(pm.getPropertyValue(APP_PATH_CSS) +
                File.separator + pm.getPropertyValue(POPUP_CSS));
        messagePane.getStylesheets().add(cssResource.toExternalForm());

        this.setScene(messageScene);
        messageScene.setOnKeyPressed(event ->
        {
            switch(event.getCode())
            {
                case ENTER:
                    username = name.getText();
                    password = pw.getText();
                    login = true;
                case ESCAPE:
                    name.clear();
                    pw.clear();
                    name.requestFocus();
                    this.close();
                    break;

                default:
            }

        });
    }

    public boolean login()
    {
        if(login)
        {
            login = false;
            return true;
        }
        return login;
    }
}
