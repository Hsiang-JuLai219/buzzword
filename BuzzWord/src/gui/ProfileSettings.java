package gui;

import controller.ProfileSettingsController;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import propertymanager.PropertyManager;

import static buzzword.Properties.*;

/**
 * @author Hsiang-Ju Lai
 */
public class ProfileSettings extends BasicScreen{

    ProfileSettingsController controller;

    //heading pane
    Label title;

    //center pane
    VBox centerPane;
    Label nameLabel;
    Label passwordLabel;
    Label confirmLabel;

    Label nameHint;
    Label passwordHint;
    Label confirmHint;

    TextField username;
    PasswordField password;
    PasswordField confirmPassword;

    VBox nameField;
    VBox passwordField;
    VBox confirmField;

    //control pane
    Button editProfile;
    Button confirm;
    Button cancel;

    //bottom pane
    TextArea records;


    public ProfileSettings()
    {
        super();
        setStyle("-fx-background-color: grey");
        centerPane = new VBox(20);
        centerPane.setAlignment(Pos.TOP_CENTER);
        setCenter(centerPane);
    }

    @Override
    public void initLayout()
    {
        initCenterPane();
        initHCBPanes();
        setupHandlers();
    }

    private void initCenterPane()
    {
        PropertyManager pm = PropertyManager.getManager();
        nameLabel = new Label(pm.getPropertyValue(PROFILE_USERNAME));
        passwordLabel = new Label(pm.getPropertyValue(PROFILE_PASSWORD));
        confirmLabel = new Label(pm.getPropertyValue(CONFIRM_PASSWORD));

        nameLabel.setId("profile-label");
        passwordLabel.setId("profile-label");
        confirmLabel.setId("profile-label");

        nameHint = new Label();
        passwordHint = new Label();
        confirmHint = new Label();

        nameHint.setId("profile-hint");
        passwordHint.setId("profile-hint");
        confirmHint.setId("profile-hint");

        username = new TextField();
        password = new PasswordField();
        confirmPassword = new PasswordField();

        username.setId("profile-textfield");
        password.setId("profile-textfield");
        confirmPassword.setId("profile-textfield");

        nameField = new VBox(5);
        nameField.setAlignment(Pos.CENTER);
        nameField.getChildren().setAll(nameLabel, username, nameHint);

        passwordField = new VBox(5);
        passwordField.setAlignment(Pos.CENTER);
        passwordField.getChildren().setAll(passwordLabel, password, passwordHint);

        confirmField = new VBox(5);
        confirmField.setAlignment(Pos.CENTER);
        confirmField.getChildren().setAll(confirmLabel, confirmPassword, confirmHint);

        centerPane.getChildren().setAll(nameField, passwordField, confirmField);

        updatedTextFields(controller.isLoggedIn(), !controller.isLoggedIn());

    }

    private void initHCBPanes()
    {
        PropertyManager pm = PropertyManager.getManager();

        editProfile = new Button(pm.getPropertyValue(EDIT_PROFILE));
        confirm = new Button(pm.getPropertyValue(CONFIRM));
        cancel = new Button(pm.getPropertyValue(CANCEL));
        title = new Label();
        title.setId("profile-title");
        headingPane.getChildren().setAll(title);
        controlPane.setMinSize(250, 100);
        displayPane.setMinSize(250, 0);
        controlPane.setMaxSize(250, 200);

        records = new TextArea();
        records.setEditable(false);
        records.setId("records");
        bottomPane.getChildren().setAll(records);
        bottomPane.setAlignment(Pos.TOP_CENTER);
        bottomPane.setPrefHeight(300);

        updateHCBPanes();
    }

    public void updatedTextFields(boolean isLoggedIn, boolean isEditing)
    {
        username.setEditable(!isLoggedIn);
        password.setEditable(isEditing);
        confirmField.setVisible(isEditing);

        if(isLoggedIn)
        {
            username.setText(controller.getUsername());
            password.setText("        ");
            if(isEditing)
                password.requestFocus();
        }


    }

    public void updateHCBPanes()
    {
        PropertyManager pm = PropertyManager.getManager();
        if(controller.isLoggedIn() && !controller.isEditing())
        {
            controlPane.getChildren().setAll(editProfile, cancel);
            title.setText(controller.getUsername() + pm.getPropertyValue(PROFILE));
            controller.putUserRecords(records);
            records.setVisible(true);
        }
        else
        {
            controlPane.getChildren().setAll(confirm, cancel);
            records.setVisible(false);
            if(controller.isEditing())
                title.setText(pm.getPropertyValue(CHANGE_PASSWORD));
            else
                title.setText(pm.getPropertyValue(CREATE_NEW_PROFILE));
        }
    }

    public String getPassword(){return password.getText();}

    public String getUsername(){return username.getText();}

    public void showUsernameHint(String hint){nameHint.setText(hint);}

    public void showPasswordHint(String hint){passwordHint.setText(hint);}

    public void showConfirmHint(String hint){confirmHint.setText(hint);}

    public boolean passwordsMatch(){return password.getText().equals(confirmPassword.getText());}

    @Override
    public void setupHandlers()
    {
        editProfile.setOnAction(event -> controller.editProfile());
        confirm.setOnAction(event -> controller.endAndSave());
        cancel.setOnAction(event -> controller.end());
    }


}
