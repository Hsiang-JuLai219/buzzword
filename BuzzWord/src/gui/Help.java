package gui;

import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import propertymanager.PropertyManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static buzzword.Properties.HELP_TEXT_PATH;
import static buzzword.Properties.HOME;

/**
 * @author Hsiang-Ju Lai
 */
public class Help extends BasicScreen{

    Workspace workspace;
    boolean initialized;

    //control pane
    Button home;

    //in replace of grid
    TextArea content;

    public Help(Workspace workspace)
    {
        super();
        this.workspace = workspace;
        initialized = false;
    }


    @Override
    public void setupHandlers() {home.setOnAction(event -> workspace.toHomeScreen());}

    @Override
    public void initLayout()
    {
        PropertyManager pm = PropertyManager.getManager();
        headingPane.setMinSize(0, 0);

        home = new Button(pm.getPropertyValue(HOME));
        VBox space = new VBox();
        space.setMinSize(100, 120);
        controlPane.getChildren().setAll(space, home);

        content = new TextArea();
        content.setId("help");
        content.setEditable(false);
        textGrid.getChildren().setAll(content);
        displayHelp();
        setupHandlers();
        initialized = true;
    }

    private void displayHelp()
    {
        String path = PropertyManager.getManager().getPropertyValue(HELP_TEXT_PATH);
        File file = new File(this.getClass().getClassLoader().getResource(path).getFile());
        try {content.setText(new Scanner(file).useDelimiter("\\A").next());}
        catch (FileNotFoundException e) {e.printStackTrace();}
    }

}
