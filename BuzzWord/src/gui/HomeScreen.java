package gui;

import controller.HomeController;
import data.GameMode;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import propertymanager.PropertyManager;

import static buzzword.Properties.*;

/**
 * @author Hsiang-Ju Lai
 */
public class HomeScreen extends BasicScreen {

    HomeController controller;

    //heading pane

    //control pane
    //before login
    Button createProfile;
    //after login
    Button user;
    MenuButton selectMode;
    MenuItem[] modes;
    Button startGame;
    //always
    Button login;
    Button help;

    //for girds
    DropShadow shadow;

    public HomeScreen() {super();}


    @Override
    public void initLayout()
    {
        initButtonLayout();
        setupHandlers();
        updateControlPane(false, null);
        initLetters();
        initGrid(false);
    }

    @Override
    public void setupHandlers()
    {
        login.setOnAction(event -> controller.login());
        createProfile.setOnAction(event -> controller.profileSetting());
        user.setOnAction(e -> controller.profileSetting());
        controller.initMenuItemHandlers(modes);
        startGame.setOnAction(event -> controller.levelSelection());
        help.setOnAction(event -> controller.help());
    }

    private void initButtonLayout()
    {
        PropertyManager pm = PropertyManager.getManager();

        createProfile = new Button(pm.getPropertyValue(CREATE_NEW_PROFILE));
        login = new Button();
        user = new Button();
        startGame = new Button(pm.getPropertyValue(START_GAME));
        help = new Button(pm.getPropertyValue(HELP));

        //select mode button init
        modes = new MenuItem[4];
        for(int i = 0; i < GameMode.values().length; i++)
            modes[i] = new MenuItem(pm.getPropertyValue(GameMode.values()[i]));

        selectMode = new MenuButton();
        selectMode.getItems().setAll(modes);

        //tooltips init
        createProfile.setTooltip(new Tooltip(pm.getPropertyValue(CREATE_NEW_TOOLTIP)));
        user.setTooltip(new Tooltip(pm.getPropertyValue(USER_TOOLTIP)));
        selectMode.setTooltip(new Tooltip(pm.getPropertyValue(SELECT_MODE_TOOLTIP)));
        startGame.setTooltip(new Tooltip(pm.getPropertyValue(START_GAME_TOOLTIP)));
        help.setTooltip(new Tooltip(pm.getPropertyValue(HELP_TOOLTIP)));
    }

    private void initLetters()
    {
        letters = new char[rows][GRID_COLUMNS];
        char[] message = "BUZZWORD".toCharArray();
        int pos = 0;
        for(int i = 0; i < rows; i++)
            for(int j = 0; j < GRID_COLUMNS; j++)
            {
                if(((i <= 1)&&(j <= 1)) || i*j >=4)
                    letters[i][j] = message[pos++];
                else
                    letters[i][j] = ' ';
            }
    }

    public void modeSelected(GameMode mode){selectMode.setText(PropertyManager.getManager().getPropertyValue(mode));}

    public void updateControlPane(boolean isLoggedIn, String userName)
    {
        PropertyManager pm = PropertyManager.getManager();
        selectMode.setText(pm.getPropertyValue(SELECT_MODE));
        startGame.setDisable(true);
        if(isLoggedIn)
        {
            user.setText(userName);
            login.setText(pm.getPropertyValue(LOGOUT));
            login.setTooltip(new Tooltip(pm.getPropertyValue(LOGOUT_TOOLTIP)));
            controlPane.getChildren().setAll(user, login, selectMode, startGame);
        }
        else
        {
            login.setText(pm.getPropertyValue(LOGIN));
            login.setTooltip(new Tooltip(pm.getPropertyValue(LOGIN_TOOLTIP)));
            controlPane.getChildren().setAll(createProfile, login, help);
        }

    }

    public void enableStartGame(){startGame.setDisable(false);}
}
