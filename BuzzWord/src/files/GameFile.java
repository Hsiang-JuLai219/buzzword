package files;

import apptemplate.AppTemplate;
import buzzword.BuzzWord;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import components.AppDataComponent;
import components.AppFileComponent;
import data.UserData;
import gui.Workspace;
import propertymanager.PropertyManager;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import static buzzword.Properties.QUIT_APPLICATION;

/**
 * @author Hsiang-Ju Lai
 */
public class GameFile implements AppFileComponent {

    AppTemplate app;

    public GameFile(AppTemplate app){this.app = app;}

    @Override
    public void saveData(AppDataComponent data, String filePath)
    {
        ObjectMapper mapper = new ObjectMapper();
        HashMap<String, int[]> records = ((UserData)data).getRecords();

        try
        {
            String to = BuzzWord.class.getResource("/").getFile() + filePath;
            mapper.writeValue(new File(to), records);
        }
        catch(IOException | NullPointerException e){System.out.println("Fail to save user data!");}
    }

    @Override
    public void loadData(AppDataComponent data, String filePath)
    {
        ObjectMapper mapper = new ObjectMapper();
        HashMap records = null;
        TypeReference<HashMap<String, int[]>> typeRef = new TypeReference<HashMap<String, int[]>>() {};
        try {records = mapper.readValue(BuzzWord.class.getClassLoader().getResource(filePath), typeRef);}
        catch (Exception e){System.out.println("First time user.");}
        if(records != null)
            ((UserData)data).setRecords(records);
        else{
            ((UserData)data).initRecords();
            saveData(data, filePath);
        }

    }

    @Override
    public void exitApplication()
    {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        YesNoCancelDialogSingleton dialog = YesNoCancelDialogSingleton.getSingleton();
        boolean stateChanged = workspace.pauseGamePlay(true);
        dialog.disableCancel(true);
        dialog.show("", PropertyManager.getManager().getPropertyValue(QUIT_APPLICATION));


        if(dialog.getSelection().equals("Yes"))
            System.exit(0);
        else
            if(stateChanged)
                ((Workspace)app.getWorkspaceComponent()).pauseGamePlay(false);
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) {}
}
