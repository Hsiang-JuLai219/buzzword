package files;

import buzzword.BuzzWord;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import propertymanager.PropertyManager;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static buzzword.Properties.ACCOUNT_LIST_PATH;

/**
 * @author Hsiang-Ju Lai
 */
public class AccountList {

    private Set<String> usernames;
    private HashMap<String, byte[]> passwords;

    @JsonIgnore
    private static AccountList accountList = null;

    public static AccountList getSingleton()
    {
        if(accountList == null)
            loadList();
        return accountList;
    }

    private AccountList()
    {
        usernames = new HashSet<>();
        passwords = new HashMap<>();
    }

    public void addUser(String username, String password)
    {
        usernames.add(username);
        passwords.put(username, encode(password));
        saveList();
    }

    public boolean checkPassword(String username, String password)
    {
        return usernames.contains(username) && Arrays.equals(passwords.get(username), encode(password));
    }

    public boolean checkUser(String name)
    {
        for(String username: usernames)
            if(username.toLowerCase().equals(name.toLowerCase()))
                return true;
        return false;
    }


    public void changePassword(String username, String newPassword)
    {
        passwords.replace(username, encode(newPassword));
        saveList();
    }

    private void saveList()
    {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        try
        {
            String path = PropertyManager.getManager().getPropertyValue(ACCOUNT_LIST_PATH);
            URL url = getClass().getClassLoader().getResource(path);
            mapper.writeValue(new File(url.getFile()), this);
        }
        catch (IOException ioe){ioe.printStackTrace();}
    }

    private static void loadList()
    {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        try
        {
            accountList =  mapper.readValue(BuzzWord.class.getClassLoader().getResource
                    (PropertyManager.getManager().getPropertyValue(ACCOUNT_LIST_PATH)), AccountList.class);
        }
        catch (NullPointerException|IOException e){accountList = new AccountList();}

        if(accountList == null)
            accountList = new AccountList();
    }

    private static byte[] encode(String message)
    {
        byte[] encodedMessage = null;
        try {encodedMessage = MessageDigest.getInstance("MD5").digest(message.getBytes("UTF-8"));}
        catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {e.printStackTrace();}
        return encodedMessage;
    }
}
