package components;

/**
 * This interface provides the structure for file components in
 * our applications. Note that by doing so we make it possible
 * for customly provided descendent classes to have their methods
 * called from this framework.
 *
 * @author Richard McKenna, Ritwik Banerjee, Hsiang-Ju Lai
 */
public interface AppFileComponent {

    void saveData(AppDataComponent data, String filePath);

    void loadData(AppDataComponent data, String filePath);

    void exitApplication();

    void exportData(AppDataComponent data, String filePath);
}
